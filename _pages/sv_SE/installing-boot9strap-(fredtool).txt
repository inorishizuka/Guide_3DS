---
title: "Installing Boot9strap (Fredtool)"
---

{% include toc title="Table of Contents" %}

### Obligatorisk läsning

This method of using Seedminer for further exploitation uses your `movable.sed` file to decrypt any DSiWare eShop title for the purposes of injecting an exploitable DSiWare title into the DS Internet Settings application. This requires you to already own (or download / buy) a DSiWare game from the eShop.

This is a currently working implementation of the "FIRM partitions known-plaintext" exploit detailed [here](https://www.3dbrew.org/wiki/3DS_System_Flaws).

To use the [magnet](https://wikipedia.org/wiki/Magnet_URI_scheme) links on this page, you will need a torrent client like [Deluge](http://dev.deluge-torrent.org/wiki/Download).

To extract the `.7z` and `.rar` files linked on this page, you will need a file archiver like [7-Zip](http://www.7-zip.org/) or [The Unarchiver](https://theunarchiver.com/).

### What You Need

* Any eShop DSiWare game
  + JPN region users can download the free title "ほぼ日の健康手帳™" ([![]({{ "/images/qrcodes/50010000005133.png" | absolute_url }}){: height="24px" width="24px"})]({{ "/images/qrcodes/50010000005133.png" | absolute_url }})
  + USA region users can download the free title "Nintendo Fan Network" ([![]({{ "/images/qrcodes/50010000013696.png" | absolute_url }}){: height="24px" width="24px"})]({{ "/images/qrcodes/50010000013696.png" | absolute_url }})
* Your `movable.sed` file from completing [Seedminer](seedminer)
* <i class="fa fa-magnet" aria-hidden="true" title="This is a magnet link. Use a torrent client to download the file."></i> - [frogcert.bin](magnet:?xt=urn:btih:d12278ea50bb3574f1fbd327f3d0e2292c70941f&dn=frogcert.bin&tr=https%3a%2f%2ftracker.fastdownload.xyz%3a443%2fannounce&tr=https%3a%2f%2fopentracker.xyz%3a443%2fannounce&tr=http%3a%2f%2fopen.trackerlist.xyz%3a80%2fannounce&tr=http%3a%2f%2ft.nyaatracker.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.tiny-vps.com%3a6969%2fannounce&tr=udp%3a%2f%2fopen.demonii.si%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.port443.xyz%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.vanitycore.co%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.torrent.eu.org%3a451%2fannounce&tr=udp%3a%2f%2fretracker.lanta-net.ru%3a2710%2fannounce&tr=udp%3a%2f%2fthetracker.org%3a80%2fannounce&tr=http%3a%2f%2ftorrent.nwps.ws%3a80%2fannounce&tr=udp%3a%2f%2ftracker.coppersurfer.tk%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.iamhansen.xyz%3a2000%2fannounce&tr=udp%3a%2f%2fbt.xxx-tracker.com%3a2710%2fannounce&tr=http%3a%2f%2f0d.kebhana.mx%3a443%2fannounce&tr=udp%3a%2f%2fexodus.desync.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=udp%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.justseed.it%3a1337%2fannounce&tr=http%3a%2f%2ftherightsize.net%3a1337%2fannounce&tr=udp%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.internetwarriors.net%3a1337%2fannounce&tr=udp%3a%2f%2f9.rarbg.com%3a2800%2fannounce&tr=https%3a%2f%2f2.track.ga%3a443%2fannounce&tr=udp%3a%2f%2fbigfoot1942.sektori.org%3a6969%2fannounce)
* The latest release of [Frogminer_save](https://github.com/zoogie/Frogminer/releases/latest)
* The latest release of [b9sTool](https://github.com/zoogie/b9sTool/releases/latest)
* The latest release of [Luma3DS](https://github.com/AuroraWright/Luma3DS/releases/latest) *(the `.7z` file)*
* The latest release of [the Homebrew Launcher](https://github.com/fincs/new-hbmenu/releases/latest)

#### Section I - Prep Work

1. Launch System Settings on your device
1. Navigate to `Data Management` -> `DSiWare`
1. Under the "System Memory" section, select your compatible DSiWare game
1. Select "Copy", then select "OK"
  + If prompted, overwrite any existing copy
1. Stäng av din enhet
1. Insert your SD card into your computer
1. Copy your `movable.sed` file to the root of your SD card
1. Copy `boot.firm` from the Luma3DS `.7z` to the root of your SD card
1. Copy `boot.nds` (B9STool) to the root of your SD card
1. Copy `boot.3dsx` to the root of your SD card
1. Copy the `private` folder from the Frogminer_save `.zip` to the root of your SD card
1. Navigate to `<ID0>` -> `<32-character-id>` -> `Nintendo DSiWare` on your SD card
  + This `<ID0>` will be the same one that you used in [Seedminer](seedminer)
1. Copy `<8-character-id>.bin` to a folder on your computer
  + If there are multiple `<8-character-id>.bin` files, ensure you only have the one DSiWare game copied to your SD card in the `Data Management` section of System settings

#### Section II - Fredtool

1. Open [Fredtool](https://fredtool.bruteforcemovable.com/) on your computer
1. Select your `movable.sed` file for the "Your movable.sed" field
1. Select your `<8-character-id>.bin` file for the "Your dsiware.bin" field
1. Complete the "I'm not a robot" captcha
1. Select "Start"
1. Wait for the process to complete
1. When the process has completed, download your modified DSiWare archive from the site
1. Copy the new `<8-character-id>.bin` file from the `output/hax/` folder of the downloaded DSiWare archive to the `/<ID0>/<32-character-id>/Nintendo DSiWare/` folder on your SD card
  + This should be a different `<8-character-id>` than your original one
  + Do not remove the existing `<8-character-id>.bin` file
1. Sätt tillbaka ditt SD-kort i din enhet
1. Power on your device
1. Launch System Settings on your device
1. Navigate to `Data Management` -> `DSiWare`
1. Under the "SD Card" section, select the "Haxxxxxxxxx!" title
1. Select "Copy", then select "OK"
1. Return to main menu of the System Settings
1. Navigate to `Internet Settings` -> `Nintendo DS Connections`, then select "OK"
1. If the exploit was successful, your device will have loaded the JPN version of Flipnote Studio

#### Section III - Flipnote Exploit

If you would prefer a visual guide to this section, one is available [here](https://zoogie.github.io/web/flipnote_directions/).
{: .notice--info}

1. Complete the initial setup process for the launched game until you reach the main menu
  + Select the left option whenever prompted during the setup process
1. Using the touch-screen, select the large left box, then select the box with an SD card icon
1. Once the menu loads, select the face icon, then the bottom right icon to continue
1. Select the frog icon at the bottom left
  + Alternatively, press (X) or (UP) on the D-Pad depending on which is shown on the top screen
1. Select the second button along the top with a film-reel icon
1. Scroll right until reel "3/3" is selected
1. Tap the third box with the letter "A" in it
1. Scroll left until reel "1/3" is selected
1. Tap the fourth box with the letter "A" in it
1. If the exploit was successful, your device will have loaded b9sTool
1. Select "Install boot9strap" and confirm
1. Exit b9sTool, then power off your device
  + You may have to force power off by holding the power button
  + If you see the Luma Configuration screen, continue with the guide without powering off

#### Section IV - Configuring Luma3DS

1. Boot your device while holding (Select) to launch the Luma configuration menu
  + If you encounter issues launching the Luma configuration menu, [follow this troubleshooting guide](https://github.com/zoogie/b9sTool/blob/master/TROUBLESHOOTING.md)
1. Use the (A) button and the D-Pad to turn on the following:
  + **"Show NAND or user string in System Settings"**
1. Press (Start) to save and reboot
  + If you get a black screen, [follow this troubleshooting guide](troubleshooting#black-screen-on-sysnand-boot-after-installing-boot9strap)

___

### Continue to [Finalizing Setup](finalizing-setup)
{: .notice--primary}
