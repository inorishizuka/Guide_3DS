---
title: "Installing Boot9strap (Fredtool)"
---

{% include toc title="Tabla de contenidos" %}

### Lectura requerida

This method of using Seedminer for further exploitation uses your `movable.sed` file to decrypt any DSiWare eShop title for the purposes of injecting an exploitable DSiWare title into the DS Internet Settings application. This requires you to already own (or download / buy) a DSiWare game from the eShop.

Todo esto son implementaciones funcionales del exploit "Particiones FIRM de texto plano conocido" detallado [aquí](https://www.3dbrew.org/wiki/3DS_System_Flaws) (*en inglés*).

Para utilizar los enlaces [magnet](https://wikipedia.org/wiki/Magnet_URI_scheme) de esta página, vas a necesitar un cliente torrent como [Deluge](http://dev.deluge-torrent.org/wiki/Download).

Para extraer los archivos `.7z` y `.rar` enlazados esta página, necesitarás un gestor de archivos como [7-Zip](http://www.7-zip.org/) o [The Unarchiver](https://theunarchiver.com/).

### Qué necesitas

* Any eShop DSiWare game
  + JPN region users can download the free title "ほぼ日の健康手帳™" ([![]({{ "/images/qrcodes/50010000005133.png" | absolute_url }}){: height="24px" width="24px"})]({{ "/images/qrcodes/50010000005133.png" | absolute_url }})
  + USA region users can download the free title "Nintendo Fan Network" ([![]({{ "/images/qrcodes/50010000013696.png" | absolute_url }}){: height="24px" width="24px"})]({{ "/images/qrcodes/50010000013696.png" | absolute_url }})
* Your `movable.sed` file from completing [Seedminer](seedminer)
* <i class="fa fa-magnet" aria-hidden="true" title="Este es un enlace magnético. Usa un cliente torrent para descargar el archivo."></i> - [frogcert.bin](magnet:?xt=urn:btih:d12278ea50bb3574f1fbd327f3d0e2292c70941f&dn=frogcert.bin&tr=https%3a%2f%2ftracker.fastdownload.xyz%3a443%2fannounce&tr=https%3a%2f%2fopentracker.xyz%3a443%2fannounce&tr=http%3a%2f%2fopen.trackerlist.xyz%3a80%2fannounce&tr=http%3a%2f%2ft.nyaatracker.com%3a80%2fannounce&tr=udp%3a%2f%2ftracker.tiny-vps.com%3a6969%2fannounce&tr=udp%3a%2f%2fopen.demonii.si%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.port443.xyz%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.vanitycore.co%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.torrent.eu.org%3a451%2fannounce&tr=udp%3a%2f%2fretracker.lanta-net.ru%3a2710%2fannounce&tr=udp%3a%2f%2fthetracker.org%3a80%2fannounce&tr=http%3a%2f%2ftorrent.nwps.ws%3a80%2fannounce&tr=udp%3a%2f%2ftracker.coppersurfer.tk%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.iamhansen.xyz%3a2000%2fannounce&tr=udp%3a%2f%2fbt.xxx-tracker.com%3a2710%2fannounce&tr=http%3a%2f%2f0d.kebhana.mx%3a443%2fannounce&tr=udp%3a%2f%2fexodus.desync.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=udp%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.justseed.it%3a1337%2fannounce&tr=http%3a%2f%2ftherightsize.net%3a1337%2fannounce&tr=udp%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.internetwarriors.net%3a1337%2fannounce&tr=udp%3a%2f%2f9.rarbg.com%3a2800%2fannounce&tr=https%3a%2f%2f2.track.ga%3a443%2fannounce&tr=udp%3a%2f%2fbigfoot1942.sektori.org%3a6969%2fannounce)
* The latest release of [Frogminer_save](https://github.com/zoogie/Frogminer/releases/latest)
* La ultima versión de [b9sTool](https://github.com/zoogie/b9sTool/releases/latest)
* La última versión de [Luma3DS](https://github.com/AuroraWright/Luma3DS/releases/latest) *(el archivo`.7z`)*
* La última versión [del Homebrew Launcher](https://github.com/fincs/new-hbmenu/releases/latest)

#### Sección I - Preparativos

1. Entra a Configuración de la consola
1. Ve a `Data Management` -> `DSiWare`
1. Ve a la sección "Memoria de la Consola" y selecciona tu juego de DSiWare compatible
1. Selecciona "Copiar", luego "OK"
  + Si se te pide, sobreescribe la copia en caso de existir
1. Apaga tu consola
1. Inserta tu tarjeta SD en tu computadora
1. Copy your `movable.sed` file to the root of your SD card
1. Copia el archivo `boot.firm` desde el `.7z` de Luma3DS a la raíz de tu tarjeta SD
1. Copia `boot.nds` (B9STool) a la raíz de tu tarjeta SD
1. Copia `boot.3dsx` a la raíz de tu tarjeta SD
1. Copy the `private` folder from the Frogminer_save `.zip` to the root of your SD card
1. Ve a `<ID0>` -> `<ID-de-32-caracteres>` -> `Nintendo DSiWare` en tu tarjeta SD
  + This `<ID0>` will be the same one that you used in [Seedminer](seedminer)
1. Copia `<ID-de-8-caracteres>.bin` a una carpeta en tu computadora
  + If there are multiple `<8-character-id>.bin` files, ensure you only have the one DSiWare game copied to your SD card in the `Data Management` section of System settings

#### Section II - Fredtool

1. Open [Fredtool](https://fredtool.bruteforcemovable.com/) on your computer
1. Select your `movable.sed` file for the "Your movable.sed" field
1. Select your `<8-character-id>.bin` file for the "Your dsiware.bin" field
1. Completa el captcha que dice "I'm not a robot"
1. Select "Start"
1. Espera a que el procesos se complete
1. When the process has completed, download your modified DSiWare archive from the site
1. Copy the new `<8-character-id>.bin` file from the `output/hax/` folder of the downloaded DSiWare archive to the `/<ID0>/<32-character-id>/Nintendo DSiWare/` folder on your SD card
  + This should be a different `<8-character-id>` than your original one
  + Do not remove the existing `<8-character-id>.bin` file
1. Vuelve a insertar tu tarjeta SD en tu consola
1. Enciende tu consola
1. Entra a Configuración de la consola
1. Ve a `Data Management` -> `DSiWare`
1. Under the "SD Card" section, select the "Haxxxxxxxxx!" title
1. Selecciona "Copiar", luego "OK"
1. Return to main menu of the System Settings
1. Navigate to `Internet Settings` -> `Nintendo DS Connections`, then select "OK"
1. Si el exploit fue exitoso, tu consola habrá cargado la versión japonesa de Flipnote Studio

#### Section III - Flipnote Exploit

If you would prefer a visual guide to this section, one is available [here](https://zoogie.github.io/web/flipnote_directions/).
{: .notice--info}

1. Completa la configuración del juego hasta que llegues al menú principal
  + Selecciona la opción de la izquierda cuando te lo pida durante la configuración inicial
1. Utilizando la pantalla táctil, selecciona el cuadrado grande de la izquierda, luego selecciona el cuadrado con el ícono de una tarjeta SD
1. Una vez que cargue el menú, selecciona el ícono con una cara, luego el ícono abajo a la derecha de la pantalla para continuar
1. Select the frog icon at the bottom left
  + Alternatively, press (X) or (UP) on the D-Pad depending on which is shown on the top screen
1. Select the second button along the top with a film-reel icon
1. Ve hacia la derecha hasta seleccionar el carrete "3/3"
1. Toca el tercer cuadrado con una letra "A" en él
1. Ve hacia la izquierda hasta el carrete "1/3"
1. Toca el cuarto cuadrado con una letra "A" en él
1. Si el exploit fue exitoso, tu consola habrá cargado b9sTool
1. Selecciona "Install boot9strap" y confirma
1. Sal de b9sTool, luego apaga tu consola
  + Puede que tengas que forzar el apagado manteniendo pulsado el botón de encendido
  + If you see the Luma Configuration screen, continue with the guide without powering off

#### Sección IV - Configurar Luma3DS

1. Enciende tu consola mientras mantienes presionado (Select) para ingresar al menú de configuración de Luma3DS
  + Si tienes problemas al intentar iniciar el menú de configuración de Luma, [sigue esta guía de resolución de problemas](https://github.com/zoogie/b9sTool/blob/master/TROUBLESHOOTING.md)
1. Utiliza el botón (A) y las flechas direccionales para activar lo siguiente:
  + **"Show NAND or user string in System Settings"**
1. Presiona (Start) para guardar y reiniciar
  + Si la pantalla se queda en negro, [sigue la guía de problemas](troubleshooting#black-screen-on-sysnand-boot-after-installing-boot9strap)

___

### Continúa en [Finalizar instalación](finalizing-setup)
{: .notice--primary}
